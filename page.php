<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

 get_header(); ?>

 <?php get_template_part( 'template-parts/featured-image' ); ?>

 <div id="page" role="main">

 <?php do_action( 'foundationpress_before_content' ); ?>
   <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
       <header>
          <div class="entry-title">
            <h1><?php the_title(); ?></h1>
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/custom-hr.png" alt="hr">
            <?php if (is_page(10) ) { ?>
               <h5 class="text-center">(Pepper Place Location Only)</h5>
            <?php } ?>
          </div>
       </header>

       <?php if (is_page(8) ) { ?>
          <section class="about-fields">
            <!-- <div class="about-content">
              <div class="about-hours">
                <p><span class="text-bold">Summer/Fall Hours:</span> <?php // echo types_render_field( "about-hours", array("output" => "raw") ) ?></p>
              </div>
              <div class="about-phone">
                <p><span class="text-bold">Phone:</span> <?php // echo types_render_field( "about-phone", array("output" => "raw") ) ?>
              </div>
            </div> -->
            <div class="about-images">
               <div class="about-image-col">
                  <div class="about-image">
                    <?php echo types_render_field( "about-image-1", array() ) ?>
                  </div>
               </div>
               <div class="about-image-col">
                  <div class="about-image">
                    <?php echo types_render_field( "about-image-2", array() ) ?>
                  </div>
               </div>
               <div class="about-image-col">
                  <div class="about-image">
                    <?php echo types_render_field( "about-image-3", array() ) ?>
                  </div>
               </div>
            </div>
          </section>
       <?php } elseif (is_page(6)) { ?>
         <section class="locations-page-content">
            <div class="row">
               <?php $locations = array ( 'post_type' => 'location', 'posts_per_page' => 2 );
               query_posts($locations);
               while (have_posts()) : the_post(); ?>
                  <div class="medium-6 columns">
                     <div class="location-block">
                        <figure>
                           <?php echo types_render_field( "location-image", array() ) ?>
                        </figure>
                        <figcaption>
                           <h4><?php echo types_render_field( "location-name", array() ) ?></h4>
                           <div class="row collapse" data-equalizer data-equalize-on="medium">
                              <div class="medium-5 columns" data-equalizer-watch>
                                 <?php echo types_render_field( "google-map", array("output" => "raw") ) ?>
                              </div>
                              <div class="medium-4 columns" data-equalizer-watch>
                                 <?php echo types_render_field( "location-address", array() ) ?>

                                 <?php echo types_render_field( "location-hours", array() ) ?>
                              </div>
                              <a href="<?php echo types_render_field( "menu-link", array('output' => 'raw') ) ?>" class="medium-3 columns menu-btn" data-equalizer-watch target="_blank">
                                 <p>View<br>Full<br>Menu</p>
                              </a>
                           </div>
                        </figcaption>
                     </div>
                  </div>
               <?php endwhile; ?>
            </div>
         </section>
      <?php } ?>

      <section class="page-content">
         <div class="page-content-col">
            <?php the_content(); ?>
         </div>
      </section>

   </article>

 <?php do_action( 'foundationpress_after_content' ); ?>

 </div>

 <?php get_footer();
