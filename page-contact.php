<?php
/*
Template Name: Contact
*/
 get_header(); ?>

 <?php get_template_part( 'template-parts/featured-image' ); ?>

 <div id="page" role="main">

 <?php do_action( 'foundationpress_before_content' ); ?>
   <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
      <header>
         <div class="entry-title">
            <h1><?php the_title(); ?></h1>
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/custom-hr.png" alt="hr">
         </div>
      </header>
      <section class="contact-page">
         <div class="contact-page-content">
            <div class="contact-page-form">
               <?php the_content(); ?>
            </div>
            <div class="contact-page-side">
               <div class="side-padding">
                  <?php $locations = array ( 'post_type' => 'location', 'posts_per_page' => 2 );
                  query_posts($locations);
                  while (have_posts()) : the_post(); ?>
                     <h5><?php echo types_render_field( "location-name", array() ) ?></h5>
                     <?php echo types_render_field( "google-map", array() ) ?>

                     <?php echo types_render_field( "location-address", array() ) ?>

                     <?php echo types_render_field( "location-hours", array() ) ?>
                  <?php endwhile; ?>
               </div>
            </div>
         </div>
      </section>
   </article>

 <?php do_action( 'foundationpress_after_content' ); ?>

 </div>

 <?php get_footer();
