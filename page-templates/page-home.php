<?php
/*
Template Name: Home
*/
get_header(); ?>

<div id="page-home" role="main">

<?php do_action( 'foundationpress_before_content' ); ?>

<main <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
   <section class="home-hero-container">
      <div id="hero-overlay"></div>
      <div class="row home-hero-content">
         <div class="small-10 medium-8 large-6 small-centered columns">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/redcat-logo-1.png" alt="home logo">
            <div class="white-divide"></div>
            <div class="home-arrow">
               <p>come on in</p>
               <a href="#hero-anchor"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
            </div>
         </div>
      </div>
   </section>

   <article class="home-main-content" id="hero-anchor">
      <section class="section-locations">
         <?php
            $post   = get_post( 6 );
            $title = $post->post_title;
         ?>
         <div class="row">
            <div class="medium-6 medium-centered columns">
               <div class="locations-title title">
                  <h1><?php echo $title; ?></h1>
               </div>
            </div>
         </div>
         <div class="row">
            <?php $locations = array ( 'post_type' => 'location', 'posts_per_page' => 2 );
            query_posts($locations);
            while (have_posts()) : the_post(); ?>
               <div class="medium-6 columns">
                  <div class="location-block">
                     <figure>
                        <?php echo types_render_field( "location-image", array() ) ?>
                     </figure>
                     <figcaption>
                        <h4><?php echo types_render_field( "location-name", array() ) ?></h4>
                        <div class="row collapse" data-equalizer data-equalize-on="medium">
                           <div class="small-12 medium-5 columns" data-equalizer-watch>
                              <?php echo types_render_field( "google-map", array("output" => "raw") ) ?>
                           </div>
                           <div class="small-6 medium-4 columns" data-equalizer-watch>
                              <?php echo types_render_field( "location-address", array() ) ?>

                              <?php echo types_render_field( "location-hours", array() ) ?>
                           </div>
                           <a href="<?php echo types_render_field( "menu-link", array('output' => 'raw') ) ?>" class="small-6 medium-3 columns menu-btn" data-equalizer-watch target="_blank">
                              <p>View<br>Full<br>Menu</p>
                           </a>
                        </div>
                     </figcaption>
                  </div>
               </div>
            <?php endwhile; ?>
         </div>
         <div class="row">

         </div>
      </section>
      <?php wp_reset_query(); ?>

      <div class="gold-divide"></div>

      <section id="section-about">
         <?php
            $post    = get_post( 8 );
            $title   = $post->post_title;
            $excerpt = $post->post_excerpt;
         ?>
         <div class="row">
            <div class="medium-4 columns">
               <div class="about-title title">
                  <h1><?php echo $title; ?></h1>
               </div>
            </div>
            <div class="medium-8 columns about-excerpt">
               <p><?php echo $excerpt; ?> <a href="<?php the_permalink(); ?>" class="about-read-more">Read The Full Red Cat Story</a>
            </div>
         </div>
      </section>
   </article>
   <?php wp_reset_query(); ?>
   <section class="full-width carousel show-for-large">
       <div class="home-carousel">
          <div>
             <?php echo types_render_field( "home-image", array( "alt" => "home image", 'separator'=>'</div><div>') ) ?>
          </div>
       </div>
   </section>
</main>


<?php do_action( 'foundationpress_after_content' ); ?>

</div>

<?php get_footer();
