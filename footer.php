<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

		</section>
		<div id="footer-container">
			<section class="ft-top">
				<div class="row" data-equalizer data-equalize-on="medium">
					<div class="medium-6 columns ft-links" data-equalizer-watch>
						<div class="row collapse">
							<div class="small-6 columns">
								<div class="signature-title title">
									<h3><a href="/signature-lattes/">Signature<br>Lattes</a></h3>
								</div>
							</div>
							<div class="small-6 columns">
								<div class="press-title title">
									<h3><a href="/in-the-press/">Red Cat<br>In The Press</a></h3>
								</div>
							</div>
						</div>
					</div>
					<div class="medium-6 columns ft-connect" data-equalizer-watch>
						<div class="row">
							<div class="large-6 columns">
								<h3>connect with us!</h3>
							</div>
							<div class="large-6 columns">
								<ul>
									<li><a href="https://www.facebook.com/red.catbham/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
									<li><a href="https://twitter.com/theredcat_bham" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
									<li><a href="https://www.instagram.com/explore/locations/226546255/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
								</ul>
							</div>
						</div>


					</div>
				</div>
			</section>
			<footer id="footer">
				<div class="row">
					<div class="medium-6 columns copy">
						Copyright &copy; <?=date('Y'); ?> The Red Cat Coffee House. All Rights Reserved.
					</div>
					<div class="medium-6 columns">
						<span class="moxy"><a href="http://digmoxy.com" target="_blank">Moxy</a></span>
					</div>
				</div>
			</footer>
		</div>

		<?php do_action( 'foundationpress_layout_end' ); ?>

<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) == 'offcanvas' ) : ?>
		</div><!-- Close off-canvas wrapper inner -->
	</div><!-- Close off-canvas wrapper -->
</div><!-- Close off-canvas content wrapper -->
<?php endif; ?>

<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/components/slick/slick/slick.min.js"></script>

<?php wp_footer(); ?>
<?php do_action( 'foundationpress_before_closing_body' ); ?>
</body>
</html>
